import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'dart:convert';
import 'memberView.dart';

import 'members.dart';

class MainViewsState extends State<MainViews> {
	final TextStyle _biggerFont = const TextStyle(fontSize: 18.0);
	var _members = <Member>[];

	@override
	  void initState() {
		super.initState();
		_loadData();
	  }

	@override
	Widget build(BuildContext context) {
		return _buildView();
	}

	Widget _buildView() {
		if (_showLoading()) {
			return Center(child: CircularProgressIndicator(),);
		} else {
			return ListView.builder(
				padding: EdgeInsets.all(16.0),
				itemCount: _members.length,
				itemBuilder: (BuildContext context, int position) {
					if (position.isOdd) return Divider();
					final index = position ~/ 2;
					return _buildRow(index);
				}
			);
		}
	}

	Widget _buildRow(int i) {
		return ListTile(
			title: Text("${_members[i].login}", style: _biggerFont),
			leading: new CircleAvatar(
				backgroundColor: Colors.green,
				backgroundImage: new NetworkImage(_members[i].avatarURL),
			),
			onTap: () {
				_pushMember(_members[i]);
			},
		);
	}

	_showLoading() {
		return _members.length == 0;
	}

  	_loadData() async {
		String dataURL = "https://api.github.com/orgs/raywenderlich/members";
		http.Response response = await http.get(dataURL);
		setState(() {
			final membersJSON = json.decode(response.body);
			for (var memberJSON in membersJSON) {
				final member = new Member(login: memberJSON["login"], avatarURL: memberJSON["avatar_url"]);
				_members.add(member);
			}
		});
	}

	_pushMember(Member member) {
		Navigator.of(context).push(
			new MaterialPageRoute(
				builder: (context) => new MemberView(member)
			)
		);
	}
}

class MainViews extends StatefulWidget {
	@override
	createState() {
		return MainViewsState();
	}
}