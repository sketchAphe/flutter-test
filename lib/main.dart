import 'package:flutter/material.dart';
import 'strings.dart';
import 'customAppBar.dart';
import 'mainView.dart';

void main() {
  return runApp(MainTemanBelanja());
}

class MainTemanBelanja extends StatelessWidget {

	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			home: Scaffold(
				appBar: CustomAppBar(Strings.appTitle),
				body: MainViews()
    		),
			theme: ThemeData(primaryColor: Colors.green.shade800),
		);
	}
}