import 'package:flutter/material.dart';
import 'memberView.dart';

class CustomAppBarState extends State<CustomAppBar> {
	final String title;

	CustomAppBarState(this.title);

  	@override
  	Widget build(BuildContext context) {
		return  AppBar(
			title: Text(title),
			actions: actionButton(),
		);
	}

	List<Widget> actionButton() {
		return [
			IconButton(
				icon: Icon(Icons.shopping_cart),
				onPressed: _cartPressed,
				)
		];
	}

	void _cartPressed() {
		Navigator.of(context).push(
			MaterialPageRoute(builder: (context) => MemberView(null))
		);
	}
}

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
	final String title;

	CustomAppBar(this.title);

	@override
	createState() {
		return CustomAppBarState(title);
	}

	@override
	Size get preferredSize => Size.fromHeight(56);
}
