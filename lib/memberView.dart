import 'package:flutter/material.dart';
import 'members.dart';
import 'customAppBar.dart';

class MemberViewState extends State<MemberView> {
	final Member member;

	MemberViewState(this.member);

	@override
	  Widget build(BuildContext context) {
		return Scaffold(
			appBar: CustomAppBar(member.login),
			body: Padding(
				padding: EdgeInsets.all(16),
				child: Column(
					children: <Widget>[
						Image.network(member.avatarURL),
						IconButton(
							icon: Icon(Icons.arrow_back,
							color: Colors.green.shade400,
							size: 48,),
							onPressed: () {
								Navigator.pop(context);
							},
						)
					],
				),
			),
		);
	  }
}

class MemberView extends StatefulWidget {
	final Member member;

	MemberView(this.member) {
		if (member == null) {
			throw new ArgumentError("member of MemberWidget cannot be null. " "Received: '$member'");
		}
	}

	@override
	State<StatefulWidget> createState() {
		return MemberViewState(member);
	}
}
