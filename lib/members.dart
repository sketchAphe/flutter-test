class Member {
	final String login;
	final String avatarURL;

	Member({this.login, this.avatarURL}) {
		login == null ? throw new ArgumentError("login cannot be null. " "Received: '$login'") : print("");
    	avatarURL == null ? throw new ArgumentError("avatar cannot be null. " "Received '$avatarURL") : print("");
	}
}